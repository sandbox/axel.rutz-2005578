<?php
/**
 * @file
 * mm_feature_imagecaption.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mm_feature_imagecaption_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_alt_title';
  $strongarm->value = 'title';
  $export['jcaption_alt_title'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_animate';
  $strongarm->value = 0;
  $export['jcaption_animate'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_autoWidth';
  $strongarm->value = 1;
  $export['jcaption_autoWidth'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_copyAlignmentToClass';
  $strongarm->value = 0;
  $export['jcaption_copyAlignmentToClass'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_copyClassToClass';
  $strongarm->value = 1;
  $export['jcaption_copyClassToClass'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_copyFloatToClass';
  $strongarm->value = 1;
  $export['jcaption_copyFloatToClass'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_copyStyle';
  $strongarm->value = 1;
  $export['jcaption_copyStyle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_hideDuration';
  $strongarm->value = '200';
  $export['jcaption_hideDuration'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_keepLink';
  $strongarm->value = 0;
  $export['jcaption_keepLink'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_removeAlign';
  $strongarm->value = 1;
  $export['jcaption_removeAlign'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_removeClass';
  $strongarm->value = 1;
  $export['jcaption_removeClass'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_removeStyle';
  $strongarm->value = 1;
  $export['jcaption_removeStyle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_requireText';
  $strongarm->value = 1;
  $export['jcaption_requireText'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_selectors';
  $strongarm->value = '.content .content img';
  $export['jcaption_selectors'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_showDuration';
  $strongarm->value = '200';
  $export['jcaption_showDuration'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jcaption_styleMarkup';
  $strongarm->value = '';
  $export['jcaption_styleMarkup'] = $strongarm;

  return $export;
}
