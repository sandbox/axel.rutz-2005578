<?php
/**
 * @file
 * mm_permissions_nodetype_access_presets.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_nodetype_access_presets_user_default_permissions() {
  $permissions = array();

  // Exported permission: view any * content.
  $permissions['view any * content'] = array(
    'name' => 'view any * content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'permission_preset',
  );

  return $permissions;
}
