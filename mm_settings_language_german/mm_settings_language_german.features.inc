<?php
/**
 * @file
 * mm_settings_language_german.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_settings_language_german_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
