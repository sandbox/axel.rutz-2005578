<?php
/**
 * @file
 * mm_permissions_pagetree.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_pagetree_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer main-menu menu items.
  $permissions['administer main-menu menu items'] = array(
    'name' => 'administer main-menu menu items',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'menu_admin_per_menu',
  );

  // Exported permission: administer menu-auxiliary-menu menu items.
  $permissions['administer menu-auxiliary-menu menu items'] = array(
    'name' => 'administer menu-auxiliary-menu menu items',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'menu_admin_per_menu',
  );

  // Exported permission: create mm_page content.
  $permissions['create mm_page content'] = array(
    'name' => 'create mm_page content',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any mm_page content.
  $permissions['delete any mm_page content'] = array(
    'name' => 'delete any mm_page content',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any mm_page content.
  $permissions['edit any mm_page content'] = array(
    'name' => 'edit any mm_page content',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
