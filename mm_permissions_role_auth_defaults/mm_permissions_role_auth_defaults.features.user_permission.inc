<?php
/**
 * @file
 * mm_permissions_role_auth_defaults.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_role_auth_defaults_user_default_permissions() {
  $permissions = array();

  // Exported permission: cancel account.
  $permissions['cancel account'] = array(
    'name' => 'cancel account',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'user',
  );

  return $permissions;
}
