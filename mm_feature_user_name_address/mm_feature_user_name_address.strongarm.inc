<?php
/**
 * @file
 * mm_feature_user_name_address.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mm_feature_user_name_address_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'realname_pattern';
  $strongarm->value = '[user:name-field-mm-name]';
  $export['realname_pattern'] = $strongarm;

  return $export;
}
