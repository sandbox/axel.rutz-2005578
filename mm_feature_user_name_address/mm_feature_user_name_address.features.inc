<?php
/**
 * @file
 * mm_feature_user_name_address.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_feature_user_name_address_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
