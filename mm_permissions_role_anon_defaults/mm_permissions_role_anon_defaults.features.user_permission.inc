<?php
/**
 * @file
 * mm_permissions_role_anon_defaults.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_role_anon_defaults_user_default_permissions() {
  $permissions = array();

  // Exported permission: access content.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'Technik' => 'Technik',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: use text format mm_wysiwyg_filter.
  $permissions['use text format mm_wysiwyg_filter'] = array(
    'name' => 'use text format mm_wysiwyg_filter',
    'roles' => array(
      'Technik' => 'Technik',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
