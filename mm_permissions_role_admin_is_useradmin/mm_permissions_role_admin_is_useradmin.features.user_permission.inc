<?php
/**
 * @file
 * mm_permissions_role_admin_is_useradmin.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_role_admin_is_useradmin_user_default_permissions() {
  $permissions = array();

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'user',
  );

  // Exported permission: administer account settings.
  $permissions['administer account settings'] = array(
    'name' => 'administer account settings',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'user_settings_access',
  );

  // Exported permission: administer users.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'user',
  );

  // Exported permission: assign Admin role.
  $permissions['assign Admin role'] = array(
    'name' => 'assign Admin role',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'role_delegation',
  );

  // Exported permission: assign Admin umschalten role.
  $permissions['assign Admin umschalten role'] = array(
    'name' => 'assign Admin umschalten role',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'role_delegation',
  );

  // Exported permission: assign Technik umschalten role.
  $permissions['assign Technik umschalten role'] = array(
    'name' => 'assign Technik umschalten role',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'role_delegation',
  );

  // Exported permission: assign all roles.
  $permissions['assign all roles'] = array(
    'name' => 'assign all roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'role_delegation',
  );

  // Exported permission: cancel users with no custom roles.
  $permissions['cancel users with no custom roles'] = array(
    'name' => 'cancel users with no custom roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Admin.
  $permissions['cancel users with role Admin'] = array(
    'name' => 'cancel users with role Admin',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Admin and other roles.
  $permissions['cancel users with role Admin and other roles'] = array(
    'name' => 'cancel users with role Admin and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Adminumschalten.
  $permissions['cancel users with role Adminumschalten'] = array(
    'name' => 'cancel users with role Adminumschalten',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Adminumschalten and other roles.
  $permissions['cancel users with role Adminumschalten and other roles'] = array(
    'name' => 'cancel users with role Adminumschalten and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Technik.
  $permissions['cancel users with role Technik'] = array(
    'name' => 'cancel users with role Technik',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Technik and other roles.
  $permissions['cancel users with role Technik and other roles'] = array(
    'name' => 'cancel users with role Technik and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Technikumschalten.
  $permissions['cancel users with role Technikumschalten'] = array(
    'name' => 'cancel users with role Technikumschalten',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role Technikumschalten and other roles.
  $permissions['cancel users with role Technikumschalten and other roles'] = array(
    'name' => 'cancel users with role Technikumschalten and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role anonymoususer.
  $permissions['cancel users with role anonymoususer'] = array(
    'name' => 'cancel users with role anonymoususer',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role anonymoususer and other roles.
  $permissions['cancel users with role anonymoususer and other roles'] = array(
    'name' => 'cancel users with role anonymoususer and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role authenticateduser.
  $permissions['cancel users with role authenticateduser'] = array(
    'name' => 'cancel users with role authenticateduser',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: cancel users with role authenticateduser and other roles.
  $permissions['cancel users with role authenticateduser and other roles'] = array(
    'name' => 'cancel users with role authenticateduser and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: create users.
  $permissions['create users'] = array(
    'name' => 'create users',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with no custom roles.
  $permissions['edit users with no custom roles'] = array(
    'name' => 'edit users with no custom roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Admin.
  $permissions['edit users with role Admin'] = array(
    'name' => 'edit users with role Admin',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Admin and other roles.
  $permissions['edit users with role Admin and other roles'] = array(
    'name' => 'edit users with role Admin and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Adminumschalten.
  $permissions['edit users with role Adminumschalten'] = array(
    'name' => 'edit users with role Adminumschalten',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Adminumschalten and other roles.
  $permissions['edit users with role Adminumschalten and other roles'] = array(
    'name' => 'edit users with role Adminumschalten and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Technik.
  $permissions['edit users with role Technik'] = array(
    'name' => 'edit users with role Technik',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Technik and other roles.
  $permissions['edit users with role Technik and other roles'] = array(
    'name' => 'edit users with role Technik and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Technikumschalten.
  $permissions['edit users with role Technikumschalten'] = array(
    'name' => 'edit users with role Technikumschalten',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role Technikumschalten and other roles.
  $permissions['edit users with role Technikumschalten and other roles'] = array(
    'name' => 'edit users with role Technikumschalten and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role anonymoususer.
  $permissions['edit users with role anonymoususer'] = array(
    'name' => 'edit users with role anonymoususer',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role anonymoususer and other roles.
  $permissions['edit users with role anonymoususer and other roles'] = array(
    'name' => 'edit users with role anonymoususer and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role authenticateduser.
  $permissions['edit users with role authenticateduser'] = array(
    'name' => 'edit users with role authenticateduser',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: edit users with role authenticateduser and other roles.
  $permissions['edit users with role authenticateduser and other roles'] = array(
    'name' => 'edit users with role authenticateduser and other roles',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'administerusersbyrole',
  );

  // Exported permission: manage permission locks.
  $permissions['manage permission locks'] = array(
    'name' => 'manage permission locks',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'permissions_lock',
  );

  // Exported permission: manage permissions unrestricted.
  $permissions['manage permissions unrestricted'] = array(
    'name' => 'manage permissions unrestricted',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'permissions_lock',
  );

  return $permissions;
}
