<?php
/**
 * @file
 * mm_permissions_role_admin_is_useradmin.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_permissions_role_admin_is_useradmin_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
