<?php
/**
 * @file
 * mm_field_images.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_field_images_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
}
