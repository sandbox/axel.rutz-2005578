<?php
/**
 * @file
 * mm_result_pagetree.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function mm_result_pagetree_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|mm_page|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'mm_page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_mm_images',
        1 => 'field_mm_body',
        2 => 'field_mm_files',
      ),
    ),
    'fields' => array(
      'field_mm_images' => 'ds_content',
      'field_mm_body' => 'ds_content',
      'field_mm_files' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|mm_page|default'] = $ds_layout;

  return $export;
}
