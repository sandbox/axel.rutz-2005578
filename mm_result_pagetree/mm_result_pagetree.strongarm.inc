<?php
/**
 * @file
 * mm_result_pagetree.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mm_result_pagetree_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_mm_page';
  $strongarm->value = 'edit-simplify';
  $export['additional_settings__active_tab_mm_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_mm_page';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_mm_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_mm_page';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_mm_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_mm_page';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_mm_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menutab_configuration_type_mm_page';
  $strongarm->value = array(
    0 => 'mandatory',
    1 => 'autotitle',
    2 => 'hidedescription',
    3 => 'hideweight',
  );
  $export['menutab_configuration_type_mm_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_mm_page';
  $strongarm->value = 'Save and add fields';
  $export['save_continue_mm_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'simplify_nodes_mm_page';
  $strongarm->value = array(
    0 => 'author',
    1 => 'format',
    2 => 'options',
    3 => 'revision',
  );
  $export['simplify_nodes_mm_page'] = $strongarm;

  return $export;
}
