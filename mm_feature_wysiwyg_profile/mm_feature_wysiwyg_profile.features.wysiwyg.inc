<?php
/**
 * @file
 * mm_feature_wysiwyg_profile.features.wysiwyg.inc
 */

/**
 * Implements hook_wysiwyg_default_profiles().
 */
function mm_feature_wysiwyg_profile_wysiwyg_default_profiles() {
  $profiles = array();

  // Exported profile: mm_wysiwyg_filter
  $profiles['mm_wysiwyg_filter'] = array(
    'format' => 'mm_wysiwyg_filter',
    'editor' => 'ckeditor',
    'settings' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 0,
      'theme' => 'advanced',
      'language' => 'de',
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Unlink' => 1,
          'Image' => 1,
          'Source' => 1,
          'RemoveFormat' => 1,
          'SpecialChar' => 1,
          'Format' => 1,
          'Table' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'linkit' => array(
          'linkit' => 1,
        ),
        'drupal' => array(
          'media' => 1,
          'break' => 1,
        ),
      ),
      'toolbar_loc' => 'top',
      'toolbar_align' => 'left',
      'path_loc' => 'bottom',
      'resizing' => 1,
      'default_toolbar_grouping' => 0,
      'verify_html' => 1,
      'preformatted' => 0,
      'convert_fonts_to_spans' => 1,
      'remove_linebreaks' => 1,
      'apply_source_formatting' => 0,
      'paste_auto_cleanup_on_paste' => 0,
      'block_formats' => 'p,h2,h3,blockquote',
      'css_setting' => 'theme',
      'css_path' => '',
      'css_classes' => '',
      'advanced__active_tab' => 'edit-basic',
      'buttonorder' => 'RemoveFormat,separator,Bold,Italic,Strike,separator,BulletedList,Outdent,Indent,separator,linkit,Unlink,media,Image,separator,Format,SpecialChar,separator,Scayt,break,Source',
    ),
    'rdf_mapping' => array(),
  );

  return $profiles;
}
