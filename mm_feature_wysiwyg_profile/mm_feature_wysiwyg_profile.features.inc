<?php
/**
 * @file
 * mm_feature_wysiwyg_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_feature_wysiwyg_profile_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
}
