<?php
/**
 * @file
 * mm_permissions_intranet_devdoc.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function mm_permissions_intranet_devdoc_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-devdoc.
  $menus['menu-devdoc'] = array(
    'menu_name' => 'menu-devdoc',
    'title' => 'DevDoc',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('DevDoc');


  return $menus;
}
