<?php
/**
 * @file
 * mm_permissions_intranet_devdoc.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mm_permissions_intranet_devdoc_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_mm_intranet_tech4tech';
  $strongarm->value = 'edit-menu';
  $export['additional_settings__active_tab_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_mm_intranet_tech4tech';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_mm_intranet_tech4tech';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_mm_intranet_tech4tech';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__mm_intranet_tech4tech';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '4',
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '3',
        ),
        'redirect' => array(
          'weight' => '2',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_mm_intranet_tech4tech';
  $strongarm->value = '0';
  $export['language_content_type_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_mm_intranet_tech4tech';
  $strongarm->value = array(
    0 => 'menu-devdoc',
  );
  $export['menu_options_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_mm_intranet_tech4tech';
  $strongarm->value = 'menu-devdoc:0';
  $export['menu_parent_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menutab_configuration_type_mm_intranet_tech4tech';
  $strongarm->value = array(
    0 => 'mandatory',
    1 => 'hidetitle',
    2 => 'hidedescription',
    3 => 'hideweight',
    4 => 'hideenable',
  );
  $export['menutab_configuration_type_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_mm_intranet_tech4tech';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
    2 => 'revision',
  );
  $export['node_options_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_mm_intranet_tech4tech';
  $strongarm->value = '0';
  $export['node_preview_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_mm_intranet_tech4tech';
  $strongarm->value = 0;
  $export['node_submitted_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_mm_intranet_tech4tech';
  $strongarm->value = 'Save and add fields';
  $export['save_continue_mm_intranet_tech4tech'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'simplify_nodes_mm_intranet_tech4tech';
  $strongarm->value = array();
  $export['simplify_nodes_mm_intranet_tech4tech'] = $strongarm;

  return $export;
}
