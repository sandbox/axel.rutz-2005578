<?php
/**
 * @file
 * mm_permissions_role_auth_is_author_presets.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_role_auth_is_author_presets_user_default_permissions() {
  $permissions = array();

  // Exported permission: create * content.
  $permissions['create * content'] = array(
    'name' => 'create * content',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'permission_preset',
  );

  // Exported permission: delete any * content.
  $permissions['delete any * content'] = array(
    'name' => 'delete any * content',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'permission_preset',
  );

  // Exported permission: edit any * content.
  $permissions['edit any * content'] = array(
    'name' => 'edit any * content',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'permission_preset',
  );

  return $permissions;
}
