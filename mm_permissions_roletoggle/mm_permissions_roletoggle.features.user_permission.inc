<?php
/**
 * @file
 * mm_permissions_roletoggle.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_roletoggle_user_default_permissions() {
  $permissions = array();

  // Exported permission: toggle role Admin.
  $permissions['toggle role Admin'] = array(
    'name' => 'toggle role Admin',
    'roles' => array(
      'Admin umschalten' => 'Admin umschalten',
      'Technik umschalten' => 'Technik umschalten',
    ),
    'module' => 'role_toggle',
  );

  // Exported permission: toggle role Admin umschalten.
  $permissions['toggle role Admin umschalten'] = array(
    'name' => 'toggle role Admin umschalten',
    'roles' => array(),
    'module' => 'role_toggle',
  );

  // Exported permission: toggle role Technik.
  $permissions['toggle role Technik'] = array(
    'name' => 'toggle role Technik',
    'roles' => array(
      'Technik umschalten' => 'Technik umschalten',
    ),
    'module' => 'role_toggle',
  );

  // Exported permission: toggle role Technik umschalten.
  $permissions['toggle role Technik umschalten'] = array(
    'name' => 'toggle role Technik umschalten',
    'roles' => array(),
    'module' => 'role_toggle',
  );

  return $permissions;
}
