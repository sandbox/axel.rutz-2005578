<?php
/**
 * @file
 * mm_permissions_intranet_userhelp.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_permissions_intranet_userhelp_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function mm_permissions_intranet_userhelp_node_info() {
  $items = array(
    'mm_intranet_tech4auth' => array(
      'name' => t('UserHelp'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
