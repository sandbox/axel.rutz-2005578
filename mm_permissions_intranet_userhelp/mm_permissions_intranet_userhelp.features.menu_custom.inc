<?php
/**
 * @file
 * mm_permissions_intranet_userhelp.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function mm_permissions_intranet_userhelp_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-userhelp.
  $menus['menu-userhelp'] = array(
    'menu_name' => 'menu-userhelp',
    'title' => 'UserHelp',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('UserHelp');


  return $menus;
}
