<?php
/**
 * @file
 * mm_nodetype_page.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function mm_nodetype_page_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-mm_page-field_mm_files'
  $field_instances['node-mm_page-field_mm_files'] = array(
    'bundle' => 'mm_page',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'file',
        'settings' => array(
          'field_delimiter' => ', ',
          'field_injector_field' => '',
          'field_injector_position' => 'before_paragraph_1',
        ),
        'type' => 'file_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mm_files',
    'label' => 'Files',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'field_mm_files',
      'file_extensions' => 'txt pdf',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-mm_page-field_mm_images'
  $field_instances['node-mm_page-field_mm_images'] = array(
    'bundle' => 'mm_page',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'flexslider_fields',
        'settings' => array(
          'caption' => 0,
          'field_delimiter' => '',
          'field_injector_field' => '',
          'field_injector_position' => 'before_paragraph_1',
          'optionset' => 'mm_default',
        ),
        'type' => 'flexslider',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mm_images',
    'label' => 'Images',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'field_mm_images',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '5mb',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
        ),
        'allowed_types' => array(
          0 => 0,
          'audio' => 0,
          'default' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-mm_page-field_mm_pagebody'
  $field_instances['node-mm_page-field_mm_pagebody'] = array(
    'bundle' => 'mm_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
          'field_injector_field' => '',
          'field_injector_position' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mm_pagebody',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 10,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Files');
  t('Images');

  return $field_instances;
}
