<?php
/**
 * @file
 * mm_permissions_intranet_intranet.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function mm_permissions_intranet_intranet_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-intranet.
  $menus['menu-intranet'] = array(
    'menu_name' => 'menu-intranet',
    'title' => 'Intranet',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Intranet');


  return $menus;
}
