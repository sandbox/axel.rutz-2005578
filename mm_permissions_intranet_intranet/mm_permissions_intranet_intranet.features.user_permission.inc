<?php
/**
 * @file
 * mm_permissions_intranet_intranet.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_intranet_intranet_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer menu-intranet menu items.
  $permissions['administer menu-intranet menu items'] = array(
    'name' => 'administer menu-intranet menu items',
    'roles' => array(),
    'module' => 'menu_admin_per_menu',
  );

  // Exported permission: create mm_intranet_auth4auth content.
  $permissions['create mm_intranet_auth4auth content'] = array(
    'name' => 'create mm_intranet_auth4auth content',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any mm_intranet_auth4auth content.
  $permissions['delete any mm_intranet_auth4auth content'] = array(
    'name' => 'delete any mm_intranet_auth4auth content',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own mm_intranet_auth4auth content.
  $permissions['delete own mm_intranet_auth4auth content'] = array(
    'name' => 'delete own mm_intranet_auth4auth content',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any mm_intranet_auth4auth content.
  $permissions['edit any mm_intranet_auth4auth content'] = array(
    'name' => 'edit any mm_intranet_auth4auth content',
    'roles' => array(
      'Technik' => 'Technik',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own mm_intranet_auth4auth content.
  $permissions['edit own mm_intranet_auth4auth content'] = array(
    'name' => 'edit own mm_intranet_auth4auth content',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: view any mm_intranet_auth4auth content.
  $permissions['view any mm_intranet_auth4auth content'] = array(
    'name' => 'view any mm_intranet_auth4auth content',
    'roles' => array(),
    'module' => 'nodetype_access',
  );

  return $permissions;
}
