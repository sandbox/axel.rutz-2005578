<?php
/**
 * @file
 * mm_permissions_intranet_intranet.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function mm_permissions_intranet_intranet_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-mm_intranet_auth4auth-field_mm_pagebody'
  $field_instances['node-mm_intranet_auth4auth-field_mm_pagebody'] = array(
    'bundle' => 'mm_intranet_auth4auth',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
          'field_injector_field' => '',
          'field_injector_position' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mm_pagebody',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');

  return $field_instances;
}
