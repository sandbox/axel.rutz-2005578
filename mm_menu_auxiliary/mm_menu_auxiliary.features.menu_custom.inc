<?php
/**
 * @file
 * mm_menu_auxiliary.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function mm_menu_auxiliary_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-auxiliary-menu.
  $menus['menu-auxiliary-menu'] = array(
    'menu_name' => 'menu-auxiliary-menu',
    'title' => 'Auxiliary menu',
    'description' => 'This menu is smaller than the main menu.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Auxiliary menu');
  t('This menu is smaller than the main menu.');


  return $menus;
}
