<?php
/**
 * @file
 * mm_feature_standard_url.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_feature_standard_url_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
