<?php
/**
 * @file
 * mm_option_error_messages_off.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_option_error_messages_off_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
