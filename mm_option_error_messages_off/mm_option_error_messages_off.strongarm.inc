<?php
/**
 * @file
 * mm_option_error_messages_off.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mm_option_error_messages_off_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'error_level';
  $strongarm->value = '0';
  $export['error_level'] = $strongarm;

  return $export;
}
