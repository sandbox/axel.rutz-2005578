<?php
/**
 * @file
 * mm_permissions_role_admin_is_sitebuilder.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_role_admin_is_sitebuilder_user_default_permissions() {
  $permissions = array();

  // Exported permission: access admin theme.
  $permissions['access admin theme'] = array(
    'name' => 'access admin theme',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'admin_theme',
  );

  // Exported permission: access backup and migrate.
  $permissions['access backup and migrate'] = array(
    'name' => 'access backup and migrate',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'backup_migrate',
  );

  // Exported permission: access backup files.
  $permissions['access backup files'] = array(
    'name' => 'access backup files',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'backup_migrate',
  );

  // Exported permission: access content overview.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: access contextual links.
  $permissions['access contextual links'] = array(
    'name' => 'access contextual links',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'contextual',
  );

  // Exported permission: access jcaption settings.
  $permissions['access jcaption settings'] = array(
    'name' => 'access jcaption settings',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'jcaption',
  );

  // Exported permission: access site reports.
  $permissions['access site reports'] = array(
    'name' => 'access site reports',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'system',
  );

  // Exported permission: admin_classes.
  $permissions['admin_classes'] = array(
    'name' => 'admin_classes',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'ds_ui',
  );

  // Exported permission: admin_fields.
  $permissions['admin_fields'] = array(
    'name' => 'admin_fields',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'ds_ui',
  );

  // Exported permission: admin_view_modes.
  $permissions['admin_view_modes'] = array(
    'name' => 'admin_view_modes',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'ds_ui',
  );

  // Exported permission: administer backup and migrate.
  $permissions['administer backup and migrate'] = array(
    'name' => 'administer backup and migrate',
    'roles' => array(
      'Technik' => 'Technik',
    ),
    'module' => 'backup_migrate',
  );

  // Exported permission: administer blocks.
  $permissions['administer blocks'] = array(
    'name' => 'administer blocks',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'block',
  );

  // Exported permission: administer content types.
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: administer field collections.
  $permissions['administer field collections'] = array(
    'name' => 'administer field collections',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'field_collection',
  );

  // Exported permission: administer fieldgroups.
  $permissions['administer fieldgroups'] = array(
    'name' => 'administer fieldgroups',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'field_group',
  );

  // Exported permission: administer image styles.
  $permissions['administer image styles'] = array(
    'name' => 'administer image styles',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'image',
  );

  // Exported permission: administer linkit.
  $permissions['administer linkit'] = array(
    'name' => 'administer linkit',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'linkit',
  );

  // Exported permission: administer media.
  $permissions['administer media'] = array(
    'name' => 'administer media',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'media',
  );

  // Exported permission: administer menu.
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'menu',
  );

  // Exported permission: administer meta tags.
  $permissions['administer meta tags'] = array(
    'name' => 'administer meta tags',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'metatag',
  );

  // Exported permission: administer nodes.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: administer pathauto.
  $permissions['administer pathauto'] = array(
    'name' => 'administer pathauto',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'pathauto',
  );

  // Exported permission: administer simplify.
  $permissions['administer simplify'] = array(
    'name' => 'administer simplify',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'simplify',
  );

  // Exported permission: administer site configuration.
  $permissions['administer site configuration'] = array(
    'name' => 'administer site configuration',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'system',
  );

  // Exported permission: administer views.
  $permissions['administer views'] = array(
    'name' => 'administer views',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'views',
  );

  // Exported permission: bypass node access.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'node',
  );

  // Exported permission: flush caches.
  $permissions['flush caches'] = array(
    'name' => 'flush caches',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: perform backup.
  $permissions['perform backup'] = array(
    'name' => 'perform backup',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'backup_migrate',
  );

  // Exported permission: restore from backup.
  $permissions['restore from backup'] = array(
    'name' => 'restore from backup',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'backup_migrate',
  );

  // Exported permission: view the administration theme.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'system',
  );

  return $permissions;
}
