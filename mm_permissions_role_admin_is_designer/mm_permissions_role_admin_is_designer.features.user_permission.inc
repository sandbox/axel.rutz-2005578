<?php
/**
 * @file
 * mm_permissions_role_admin_is_designer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mm_permissions_role_admin_is_designer_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer themes.
  $permissions['administer themes'] = array(
    'name' => 'administer themes',
    'roles' => array(
      'Admin' => 'Admin',
      'Technik' => 'Technik',
    ),
    'module' => 'system',
  );

  return $permissions;
}
