<?php
/**
 * @file
 * mm_field_name.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function mm_field_name_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_mm_name'
  $field_bases['field_mm_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_mm_name',
    'foreign keys' => array(),
    'indexes' => array(
      'family' => array(
        0 => 'family',
      ),
      'given' => array(
        0 => 'given',
      ),
    ),
    'locked' => 0,
    'module' => 'name',
    'settings' => array(
      'autocomplete_separator' => array(
        'credentials' => ', ',
        'family' => ' -',
        'generational' => ' ',
        'given' => ' -',
        'middle' => ' -',
        'title' => ' ',
      ),
      'autocomplete_source' => array(
        'credentials' => array(),
        'family' => array(),
        'generational' => array(
          'generational' => 0,
        ),
        'given' => array(),
        'middle' => array(),
        'title' => array(
          'title' => 'title',
        ),
      ),
      'components' => array(
        'credentials' => 0,
        'family' => 'family',
        'generational' => 0,
        'given' => 'given',
        'middle' => 0,
        'title' => 0,
      ),
      'generational_options' => '-- --
Jr.
Sen.
I
II
III
IV
V
VI
VII
VIII
IX
X',
      'labels' => array(
        'credentials' => 'Zugangsdaten',
        'family' => 'Nachname',
        'generational' => 'Generational',
        'given' => 'Vorname',
        'middle' => 'Middle name(s)',
        'title' => 'Titel',
      ),
      'max_length' => array(
        'credentials' => 255,
        'family' => 63,
        'generational' => 15,
        'given' => 63,
        'middle' => 127,
        'title' => 31,
      ),
      'minimum_components' => array(
        'credentials' => 0,
        'family' => 'family',
        'generational' => 0,
        'given' => 'given',
        'middle' => 0,
        'title' => 0,
      ),
      'profile2_private' => FALSE,
      'sort_options' => array(
        'generational' => 0,
        'title' => 'title',
      ),
      'title_options' => '-- --
Herr
Fr.
Frau
Fr.
Dr.
Prof.',
    ),
    'translatable' => 0,
    'type' => 'name',
  );

  return $field_bases;
}
