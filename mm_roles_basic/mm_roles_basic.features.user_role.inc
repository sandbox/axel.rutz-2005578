<?php
/**
 * @file
 * mm_roles_basic.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mm_roles_basic_user_default_roles() {
  $roles = array();

  // Exported role: Admin.
  $roles['Admin'] = array(
    'name' => 'Admin',
    'weight' => 2,
    'machine_name' => 'admin',
  );

  // Exported role: Admin umschalten.
  $roles['Admin umschalten'] = array(
    'name' => 'Admin umschalten',
    'weight' => 4,
    'machine_name' => 'toggle_admin',
  );

  // Exported role: Technik.
  $roles['Technik'] = array(
    'name' => 'Technik',
    'weight' => 3,
    'machine_name' => 'technician',
  );

  // Exported role: Technik umschalten.
  $roles['Technik umschalten'] = array(
    'name' => 'Technik umschalten',
    'weight' => 5,
    'machine_name' => 'toggle_technician',
  );

  return $roles;
}
